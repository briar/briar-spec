# Bramble Rendezvous Protocol, version 0

## 1 Introduction

Bramble Rendezvous Protocol (BRP) is a discovery protocol for peer-to-peer networks. It enables two peers that have previously exchanged public keys to connect to each other. No other information, such as network addresses, needs to be exchanged in advance. Instead the peers use a shared secret derived from their public and private keys to generate pseudo-random contact details for connecting to each other over various transports.

Any connections established by BRP are returned to the calling application, together with a shared key that can be used by other protocols for securing communication between the peers.

BRP is designed to operate over connection-oriented, bidirectional transport protocols. The current version of BRP uses the Tor hidden service protocol as its transport. Future versions of BRP may support other transports.

### 1.1 Motivation

BRP enables two peers in a peer-to-peer network to connect to each other by exchanging public keys. The public keys can be encoded as short text strings, which are easy to exchange over a wide range of channels, such as email or social networks.

Before they can connect, the peers need to know how to contact each other. A straightforward approach would be for the peers to encode their contact details in the same strings as their public keys. Instead, BRP uses a shared secret derived from the peers' public and private keys to generate pseudo-random contact details that are known to both peers but not to any other party. This has the following advantages:

* The amount of information that needs to be exchanged is small, so the strings are short
* The information is stable over time, so it can be exchanged asynchronously
* Contact details that might be used to identify or track the peers, now or in the future, are not exposed to other parties during the exchange

### 1.2 Adversary Model

We assume the adversary can read, modify, delete and insert traffic on all transports at will.

Practically we should not assume that the peers exchanged their public keys over a secure channel. If the adversary intercepted the prior exchange of public keys and replaced them with its own public keys then BRP can not detect or prevent man-in-the-middle attacks. (Limitation)

If the adversary knows both peers' public keys but did not replace them during the prior exchange then BRP prevents the adversary from learning the peers' network addresses, unless the adversary can see the addresses by observing the underlying transport.

BRP does not rely on lower protocol layers to provide confidentiality, integrity, authenticity, forward secrecy or concealability.

### 1.3 Notation

- || denotes concatenation

- Double quotes denote an ASCII string

- len(x) denotes the length of x in bytes

- int\_n(x) denotes x represented as an unsigned, big-endian, n-bit integer

### 1.4 Cryptographic Primitives

BRP uses four cryptographic primitives:

1. **A cryptographic hash function**, H(m)

2. **A key agreement function**, DH(pri, pub), where pri is one party's private key and pub is the other party's public key

3. **A message authentication code**, MAC(k, m), which must be a pseudo-random function

4. **A stream cipher**, STREAM(k, len), which expands a key into a stream of pseudo-random bytes

We use H(m) to define a multi-argument hash function:

- HASH(x\_1, ..., x\_n) = H(int\_32(len(x\_1)) || x\_1 || ... || int\_32(len(x\_n)) || x\_n)

We use MAC(k, m) to define a key derivation function:

- KDF(k, x\_1, ..., x\_n) = MAC(k, int\_32(len(x\_1)) || x\_1 || ... || int\_32(len(x\_n)) || x\_n)

All hashes are HASH\_LEN bytes, all symmetric keys are KEY\_LEN bytes, and the output of MAC(k, m) is MAC\_LEN bytes. For simplicity we require that HASH\_LEN = KEY\_LEN = MAC\_LEN.

(*Note:* The current version of the protocol uses BLAKE2b as the hash function and keyed BLAKE2b as the message authentication code, both with an output length of 32 bytes. This gives HASH\_LEN = KEY\_LEN = MAC\_LEN = 32. The key agreement function is X25519. The stream cipher is Salsa20 with an all-zero nonce.)

### 1.5 Prerequisites

Before two peers can communicate using BRP they must exchange public keys. BRP does not specify how this should be done. Any synchronous or asynchronous communication channel can be used, subject to the caveat that if an adversary can modify the keys being exchanged then the adversary can carry out a man-in-the-middle attack against BRP.

The peers take the roles of **Alice** and **Bob** in BRP according to the lexicographic order of their public keys, compared as byte strings. The peer with the earlier key plays the role of Alice, while the peer with the later key plays the role of Bob. The roles are symmetrical.

## 2 The Rendezvous Protocol

### 2.1 Deriving the Rendezvous Key

Both peers calculate the "raw" shared secret as follows:

- Alice calculates raw\_secret = DH(pri\_a, pub\_b)
- Bob calculates raw\_secret = DH(pri\_b, pub\_a)

(*Note:* If a peer calculates an X25519 raw shared secret that is all zeroes, the peer must abort the protocol.)

If the adversary did not intercept the prior exchange of public keys then both peers will calculate the same raw shared secret, which will be unknown to the adversary.

The peers then derive a "cooked" shared secret known as the **static master key**, which incorporates both peers' public keys:

- static\_master\_key = HASH("org.briarproject.bramble.transport/STATIC\_MASTER\_KEY", raw\_secret, pub\_a, pub\_b)

Next the peers derive a **rendezvous key** from the static master key:

- rendezvous\_key = KDF(static\_master\_key, "org.briarproject.bramble.rendezvous/RENDEZVOUS\_KEY", int\_8(protocol\_version))

The protocol version is 0 for the current version of BRP.

### 2.2 Creating Network Endpoints

Each peer uses the rendezvous key to create a **network endpoint** for each transport the peer supports. The peer derives a **stream key** for each transport from the rendezvous key. The peer uses the stream key to generate pseudo-random **contact details** for its own endpoint and the other peer's endpoint. The other peer derives the same stream key and generates the same contact details, so the peers know each other's contact details.

- stream\_key = KDF(rendezvous\_key, "org.briarproject.bramble.rendezvous/KEY\_MATERIAL", transport\_id)

The only transport used by the current version of BRP is the Tor hidden service protocol, which has the transport identifier "org.briarproject.bramble.tor".

#### 2.2.1 Network Endpoints for the Tor Hidden Service Protocol

Both peers derive 64 bytes of key material from the stream key:

- key\_material = STREAM(stream\_key, 64)

The first 32 bytes are Alice's seed and the last 32 bytes are Bob's seed. Each seed is used as the private key of an Ed25519 key pair, as described in RFC 8032 section 5.1.5. Each key pair is used as the master identity key pair of a Tor hidden service, as described in section 1.9 of the Tor Rendezvous Specification, Version 3. The hidden service address is derived from the public key, as described in section 6 of the same specification.

Each peer uses the identity key pair of its own hidden service to publish the hidden service and accept incoming connections, and at the same time uses the address of the other peer's hidden service to make outgoing connections.

### 2.3 Polling for Connections

Each peer keeps its network endpoints open and tries to connect to the other peer's endpoints once per minute for up to 48 hours. If a peer goes offline during this time, it reopens its endpoints and resumes trying to connect when it comes back online.

When BRP establishes an incoming or outgoing connection it passes the connection to the application layer. When the application layer determines that no further rendezvous connections are needed, it tells BRP to end the rendezvous. BRP closes its network endpoints and stops making outgoing connections.

If no connection is made within 48 hours of the initial key exchange, the rendezvous is considered to have failed. BRP notifies the application layer, closes its network endpoints and stops making outgoing connections.

### 2.4 Use of Rendezvous Connections

The application layer is responsible for deciding how to use any rendezvous connections created by BRP. BRP does not authenticate the remote peer, but it makes the static master key available to the application layer so that other protocols can derive keys from it, for example to encrypt and authenticate communication with the remote peer over a rendezvous connection.

Once the peers have established a secure connection they may exchange long-term contact details to replace the temporary contact details created by BRP.

## 3 Man-in-the-Middle Attacks

As noted above, if an adversary intercepted the prior exchange of public keys and replaced them with its own public keys then BRP does not prevent man-in-the-middle attacks. Each peer will derive a static master key and contact details that are known to the adversary rather than the other peer, allowing the adversary to interpose itself between the peers. The adversary will have access to any future communication between the peers that is secured with keys derived from the static master key.
