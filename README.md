# Bramble specification

The wiki has an explanatory [overview of the protocol stack](https://code.briarproject.org/briar/briar/wikis/A-Quick-Overview-of-the-Protocol-Stack)
as well as text on
[the architecture](https://code.briarproject.org/briar/briar/wikis/home#architecture).

## Formal specifications in this repo

- [Binary Data Format](BDF.md)

### Bramble Protocols

- [Bramble Handshake Protocol](protocols/BHP.md)
- [Bramble QR Code Protocol](protocols/BQP.md)
- [Bramble Rendezvous Protocol](protocols/BRP.md)
- [Bramble Synchronisation Protocol](protocols/BSP.md)
- [Bramble Transport Protocol](protocols/BTP.md)

### Briar BSP Clients

* [Transport Key Agreement Client](clients/Transport-Key-Agreement-Client.md)
* [Transport Properties Client](clients/Transport-Properties-Client.md)
* [Messaging Client](clients/Messaging-Client.md)
* [Forum Client](clients/Forum-Client.md)
* [Forum Sharing Client](clients/Forum-Sharing-Client.md)
* [Blog Client](clients/Blog-Client.md)
* [Blog Sharing Client](clients/Blog-Sharing-Client.md)
* [Private Group Client](clients/Private-Group-Client.md)
* [Private Group Sharing Client](clients/Private-Group-Sharing-Client.md)
* [Introduction Client](clients/Introduction-Client.md)
