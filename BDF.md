# Binary Data Format, version 1

Binary Data Format (BDF) is a structured data format designed for simplicity and compactness. It has six primitive types (null, boolean, integer, float, string, raw) and two container types (list, dictionary).

BDF data consists of a series of objects. The first four bits of each object indicate its type. Depending on the type, the next four bits may be unused, or may give the value, the length of the value, or the length of the length of the value. All lengths are measured in bytes.

**0: Null** - Next four bits are zero.

**1: Boolean** - Next four bits give the value, which is 0 for false or 1 for true.

**2: Integer** - Next four bits give the length, which is 1, 2, 4 or 8. The value is an integer with the specified length.

**3: Float** - Next four bits give the length, which is 8. The value is a floating point number with the specified length.

**4: String** - Next four bits give the length-of-length, which is 1, 2, 4 or 8. The length is an integer with the specified length-of-length. The value is a string with the specified length.

**5: Raw** - Next four bits give the length-of-length, which is 1, 2, 4 or 8. The length is an integer with the specified length-of-length. The value is binary data with the specified length.

**6: List** - Next four bits are zero. The list contains zero or more elements followed by an End object. Elements may be of any type except End.

**7: Dictionary** - Next four bits are zero. The dictionary contains zero or more key-value pairs followed by an End object. Keys are String objects, values may be of any type except End.

**8: End** - Next four bits are zero. Marks the end of a list or dictionary.

Integers and lengths are big-endian two's complement, floating point numbers are IEEE 754, and strings are UTF-8.

If data is to be hashed or signed, integers and lengths should be represented using the minimum number of bytes, and dictionary keys should be unique and sorted in lexicographic order.
