# Messaging Client

The messaging client is a [BSP client](/protocols/BSP.md) that synchronises private messages between pairs of devices.

### Identifier

The client's identifier is `org.briarproject.briar.messaging`. The major version is 0.

### Groups

The client uses a separate BSP group for communicating with each contact. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) containing the unique IDs of the contacts' identities, sorted in ascending order as byte strings.

### Message types

**PRIVATE_MESSAGE** - The message body is a BDF list with one element: `text` (string).

### Validity policy

* A private message is valid if it is well-formed.

### Storage policy

* All messages are stored.

### Sharing policy

* All local messages are shared.

### Security properties

Confidentiality, integrity and authenticity are provided by the transport security layer ([BTP](/protocols/BTP.md)). Messages are repudiable, but this is not a security goal and may change in future versions of the client.
