# Forum Client

The forum client is a [BSP client](/protocols/BSP.md) that synchronises forum posts among groups of devices. It is used in conjunction with the [forum sharing client](forum sharing client).

Any user who subscribes to a forum can post messages. Posts are signed by their authors.

### Identifier

The client's identifier is `org.briarproject.briar.forum`. The major version is 0.

### Groups

Each forum is represented by a separate BSP group. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) with two elements: `name` (string) and `salt` (raw). The salt is 32 random bytes, to prevent collisions between forums with the same name.

### Message types

**POST** - The message body is a BDF list with four elements: `parentId` (unique ID or null), `author` (list), `text` (string), and `signature` (raw).

`parentId` is the optional ID of a post in the same forum to which this post replies. `author` is a list with three elements: `formatVersion` (int), `nickname` (string), and `publicKey` (raw). This identifies the author of the post.

The signature covers a BDF list with five elements: `groupId` (unique ID), `timestamp` (int), `parentId` (unique ID or null), `author` (list), and `text` (string). The group ID and timestamp are taken from the message header. The public key from `author` is used to verify the signature. The signature label is `org.briarproject.briar.forum/POST`.

### Validity policy

* A post is valid if it is well-formed, has a valid signature, and its parent (if any) is valid.

### Storage policy

* All messages are stored.

### Sharing policy

* All messages are shared.

### Security properties

Confidentiality, integrity and authenticity are provided by the transport security layer ([BTP](/protocols/BTP.md)). Posts are signed with the identity key pairs of their authors and are not repudiable.
