# Introduction Client

The introduction client is a [BSP client](/protocols/BSP.md) that allows a user to introduce two contacts to each other. Each contact may accept or decline the introduction. If both contacts accept, they become each other's contacts.

### Identifier

The client's identifier is `org.briarproject.briar.introduction`. The major version is 1.

### Groups

The client uses a separate BSP group for communicating with each contact. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) containing the unique IDs of the contacts' identities, sorted in ascending order as byte strings.

### Roles

All communication happens between two contacts, who may take different roles with respect to each introduction. The contact who initiated the introduction takes the role of **introducer**. The other contact takes the role of **introducee**. Each introduction involves two introducees, who are referred to as **Alice** and **Bob**. Alice is the introducee whose unique ID is lower, comparing the IDs as byte strings.

### Sessions

The messages exchanged between a given introducer and (unordered) pair of introducees constitute a session. For the introducer, the session uses the BSP groups shared with the two introducees. For each introducee, the session uses the BSP group shared with the introducer. The introducees do not communicate directly.

The session ID is the hash of a BDF list containing the unique ID of the introducer, followed by the unique IDs of the introducees sorted in ascending order as byte strings. The hash label is `org.briarproject.briar.introduction/SESSION_ID`.

### Message types

#### Message types:

The protocol uses six message types.

**0: REQUEST** - Sent by the introducer to each introducee. The message body is a BDF list with four elements: `messageType` (int), `previousMessageId` (unique ID or null), `contact` (list), and `text` (string or null).

`previousMessageId` is the unique ID of the previous message sent by the sender in this session, if any, which is a dependency.

`contact` is the identity of the other introducee, which is a list with three elements: `formatVersion` (int), `nickname` (string), and `publicKey` (raw).

`text` is an optional message from the introducer to the introducee explaining the introduction.

**1: ACCEPT** - Sent by an introducee to the introducer if the sender accepts the introduction. Forwarded by the introducer to the other introducee. The message body is a BDF list with six elements: `messageType` (int), `sessionId` (unique ID), `previousMessageId` (unique ID), `ephemeralPublicKey` (raw), `timestamp` (int), and `transportProperties` (dictionary).

Each key of the `transportProperties` dictionary is a transport ID. The value is a dictionary containing properties for the respective transport, where the keys and values are strings. The `transportProperties` dictionary may not be empty.

**2: DECLINE** - Sent by an introducee to the introducer if the sender declines the introduction. Forwarded by the introducer to the other introducee. The message body is a BDF list with three elements: `messageType` (int), `sessionId` (unique ID), and `previousMessageId` (unique ID).

**3: AUTH** - Sent by an introducee to the introducer if accept messages have been sent and received. Forwarded by the introducer to the other introducee. The message body is a BDF list with five elements: `messageType` (int), `sessionId` (unique ID), `previousMessageId` (unique ID), `mac` (raw), and `signature` (raw).

Alice calculates the master key as `SHARED_SECRET("org.briarproject.briar.introduction/MASTER_KEY", bobEphemeralPublicKey, aliceEphemeralPrivateKey, clientMajorVersion, aliceEphemeralPublicKey, bobEphemeralPublicKey)`, where `clientMajorVersion` is the byte string `0x01`.

Bob calculates the master key as `SHARED_SECRET("org.briarproject.briar.introduction/MASTER_KEY", aliceEphemeralPublicKey, bobEphemeralPrivateKey, clientMajorVersion, aliceEphemeralPublicKey, bobEphemeralPublicKey)`. If the introducer forwards the ephemeral public keys without modifying them, this is the same value calculated by Alice.

The MAC sent by Alice covers a BDF list with three elements: `introducerId` (unique ID), `aliceInfo` (list), and `bobInfo` (list). The MAC label is `org.briarproject.briar.introduction/AUTH_MAC`. Alice's MAC key is `KDF("org.briarproject.briar.introduction/ALICE_MAC_KEY", masterKey)`.

The `aliceInfo` and `bobInfo` lists contain the information sent by Alice and Bob, respectively. Each list has four elements: `authorId` (unique ID), `timestamp` (int), `ephemeralPublicKey` (raw), and `transportProperties` (dictionary).

The signature sent by Alice is `SIGN("org.briarproject.briar.introduction/AUTH_SIGN", aliceIdentityPrivateKey, aliceNonce)`, where `aliceNonce` is `MAC("org.briarproject.briar.introduction/AUTH_NONCE", aliceMacKey)`.

The MAC sent by Bob covers a BDF list with three elements: `introducerId` (unique ID), `bobInfo` (list), and `aliceInfo` (list). The MAC label is `org.briarproject.briar.introduction/AUTH_MAC`. Bob's MAC key is `KDF("org.briarproject.briar.introduction/BOB_MAC_KEY", masterKey)`.

The signature sent by Bob is `SIGN("org.briarproject.briar.introduction/AUTH_SIGN", bobIdentityPrivateKey, bobNonce)`, where `bobNonce` is `MAC("org.briarproject.briar.introduction/AUTH_NONCE", bobMacKey)`.

Each introducee derives transport keys from the master key before sending an auth message, then deletes her ephemeral private key and the master key. The MAC keys are retained for sending and receiving activate messages.

**4: ACTIVATE** - Sent by an introducee to the introducer if auth messages have been sent and received. Forwarded by the introducer to the other introducee. The message body is a BDF list with four elements: `messageType` (int), `sessionId` (unique ID), `previousMessageId` (unique ID), and `mac` (raw).

The MAC sent by Alice is `MAC("org.briarproject.briar.introduction/ACTIVATE_MAC", aliceMacKey)`. The MAC sent by Bob is `MAC("org.briarproject.briar.introduction/ACTIVATE_MAC", bobMacKey)`.

Each introducee deletes the MAC keys after sending and receiving activate messages.

**5: ABORT** - Sent by any party to abort the protocol. The message body is a BDF list with three elements: `messageType` (int), `sessionId` (unique ID), and `previousMessageId` (unique ID).

Each introducee deletes her ephemeral private key, the master key, and the MAC keys before sending an abort message, or after receiving an abort message.

### State machines

Introducer state machine:

![introducer-state-machine-3a](/assets/clients/Introduction-Client/introducer-state-machine-3a.png)

[introducer-state-machine-3a.odg](/assets/clients/Introduction-Client/introducer-state-machine-3a.odg)

Introducee state machine:

![introducee-state-machine-3a](/assets/clients/Introduction-Client/introducee-state-machine-3a.png)

[introducee-state-machine-3a.odg](/assets/clients/Introduction-Client/introducee-state-machine-3a.odg)

### Validity policy

* A request, accept or decline message is valid if it is well-formed and its previous message (if any) is a valid message from the same sender in the same session.
* An auth message is valid if it is well-formed and its previous message (if any) is a valid message from the same sender in the same session. An introducee receiving an auth message must also validate the MAC using the other introducee's MAC key and the signature using the other introducee's identity key.
* An activate message is valid if it is well-formed and its previous message (if any) is a valid message from the same sender in the same session. An introducee receiving an activate message must also validate the MAC using the other introducee's MAC key.

### Storage policy

* All messages are stored.

### Sharing policy

* All local messages are shared.

### Security properties

Confidentiality, integrity and authenticity between the introducer and each introducee are provided by the transport security layer ([BTP](/protocols/BTP.md)).

There is no assurance of confidentiality with respect to the introducer, for any messages sent between the introducees via the introducer as part of this protocol. In other words, the introducer can read all messages she forwards between the introducees. Confidentiality of these messages with respect to any external parties is provided by the transport security layer.

There is no assurance of integrity or authenticity with respect to the introducer, for any messages sent between the introducees via the introducer as part of this protocol. In particular, the introducer may carry out a **man-in-the-middle attack** against the introducees, by introducing each introducee to an identity controlled by the introducer. In this scenario the introducees will believe that they are communicating with each other via the introducer, when in fact they are communicating with identities controlled by the introducer. The introducer can replace the transport properties sent by the introducees with transport properties controlled by the introducer, so that after the introduction has been completed the introducees will continue to communicate with the introducer instead of with each other. This allows the man-in-the-middle attack to continue against all subsequent communication between the introducees.

To carry out such an attack, the introducer must create two identities that are distinct from the identities of the introducees, and must include these identities in the initial request messages sent to the introducees.

If the introducees meet in person at any time after the introduction has been completed, they can check that the introducer did not carry out a man-in-the-middle attack by comparing each other's identities with the identities they received from the introducer.

The transport keys that the introducees derive during the introduction are used to ensure the confidentiality, integrity and authenticity of communication between the introducees after the introduction has been completed. To ensure the forward secrecy of this subsequent communication, the introducees must both delete their ephemeral private keys before using the transport keys.

Each introducee can ensure that the transport keys are not used until both ephemeral private keys have been deleted by marking the transport keys as ineligible to be used for securing outgoing BTP streams until either (a) the other introducee's activate message has been received via the introducer and its MAC has been validated, or (b) an incoming BTP stream secured with the transport keys in question has been received.

In the latter case, receipt of a stream secured with the transport keys indirectly indicates that the other introducee has received and validated this introducee's activate message and sent its own activate message, and thus the keys are safe to use, even though the other introducee's activate message may still be in transit.
