# Private Group Sharing Client

The private group sharing client is a [BSP client](/protocols/BSP.md) that allows users to share private groups with their contacts, who may accept or decline the invitations. It is used in conjunction with the [private group client](private group client).

### Identifier

The client's identifier is `org.briarproject.briar.privategroup.invitation`. The major version is 0.

### Groups

The client uses a separate BSP group for communicating with each contact. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) containing the unique IDs of the contacts' identities, sorted in ascending order as byte strings.

### Roles

All communication happens between two contacts, who may take different roles with respect to each private group. If one of the contacts created the private group, that contact takes the role of **creator** and the other takes the role of **invitee**. Otherwise both contacts take the role of **peer**. 

### Sessions

The messages exchanged between two contacts referring to a given private group constitute a session. The private group's unique ID is used as the session ID.

### Message types

The protocol uses four message types.

**0: INVITE** - Sent by the creator to the invitee. The message body is a BDF list with six elements: `messageType` (int), `creator` (list), `groupName` (string), `salt` (raw), `text` (string or null), and `signature` (raw).

`creator`, `groupName` and `salt` are taken from the private group descriptor. `creator` must be the identity of the contact who sent the message.

`text` is an optional message from the creator to the invitee explaining the invitation.

The signature covers a BDF list with three elements: `timestamp` (int), `contactGroupId` (unique ID), and `privateGroupId` (unique ID). The timestamp and contact group ID are taken from the message header. The private group ID is calculated from the private group descriptor. The public key from the private group descriptor is used to validate the signature. The signature label is `org.briarproject.briar.privategroup.invitation/INVITE`.

The creator sets the private group's visibility to VISIBLE when sending an invite message.

**1: JOIN** - Sent by an invitee in response to an invite, or by a peer when sharing the private group with a contact who is also a member. The message body is a BDF list with three elements: `messageType` (int), `privateGroupId` (unique ID), and `previousMessageId` (unique ID).

`previousMessageId` is the ID of the previous message in this session.

An invitee sets the private groups's visibility to SHARED when sending a join message. The creator sets the private group's visibility to SHARED when receiving a join message.

A peer sets the private group's visibility to VISIBLE or SHARED when sending or receiving a join message, depending on the state machine.

**2: LEAVE** - Sent by an invitee in response to an invite, or by an invitee or a peer when leaving the private group, or by the creator when dissolving the private group. The message body is a BDF list with three elements: `messageType` (int), `privateGroupId` (unique ID), and `previousMessageId` (unique ID).

`previousMessageId` is the ID of the previous message in this session.

The sender sets the private group's visibility to INVISIBLE when sending a leave message. The recipient sets the private group's visibility to INVISIBLE when receiving a leave message.

**3: ABORT** - Sent by either party when recieving a message that is valid but unexpected in the current state. The message body is a BDF list with two elements: `messageType` (int) and `privateGroupId` (unique ID).

The sender sets the private group's visibility to INVISIBLE when sending an abort message. The recipient sets the private group's visibility to INVISIBLE when receiving an abort message.

### State machines

Creator state machine:

![creator-state-machine](/assets/clients/Private-Group-Sharing-Client/creator-state-machine.png)

[creator-state-machine.odg](/assets/clients/Private-Group-Sharing-Client/creator-state-machine.odg)

Invitee state machine:

![invitee-state-machine](/assets/clients/Private-Group-Sharing-Client/invitee-state-machine.png)

[invitee-state-machine.odg](/assets/clients/Private-Group-Sharing-Client/invitee-state-machine.odg)

Peer state machine:

![peer-state-machine](/assets/clients/Private-Group-Sharing-Client/peer-state-machine.png)

[peer-state-machine.odg](/assets/clients/Private-Group-Sharing-Client/peer-state-machine.odg)

"Member announcement" in the peer state machine indicates receipt of the contact's [join message in the private group](Private-Group-Client#message-types), demonstrating that the contact is a member.

### Validity policy

* An invite message is valid if it is well-formed, is sent by the group creator, and has a valid signature from the group creator.
* A join, leave or abort message is valid if it is well-formed and its previous message (if any) is a valid message in the same session.

### Storage policy

* All messages are stored.

### Sharing policy

* All local messages are shared.

### Security properties

Confidentiality, integrity and authenticity are provided by the transport security layer ([BTP](/protocols/BTP.md)). Invite messages are signed with the identity key pair of the group creator and are not repudiable. The other messages used by this client are repudiable, but this is not a security goal and may change in future versions of the client.
