# Transport Properties Client

The transport properties client is a [BSP client](/protocols/BSP.md) that synchronises transport properties between pairs of devices. Transport properties describe how to connect to a device over various transports.

### Identifier

The client's identifier is `org.briarproject.bramble.properties`. The major version is 0.

### Groups

The client uses a separate BSP group for communicating with each contact. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) containing the unique IDs of the contacts' identities, sorted in ascending order as byte strings.

The client also uses a group with an empty descriptor for storing local transport properties. This group is not shared with any contacts.

### Message types

**UPDATE** - The message body is a BDF list with three elements: `transportId` (string), `version` (int), and `properties` (dictionary). `transportId` and `properties` are supplied by the transport plugin. The keys and values of `properties` are strings. `version` is incremented whenever the properties change.

### Validity policy

* An update is valid if it is well-formed.

### Storage policy

* In the groups shared with contacts:
    * For each transport, the local message with the highest version is stored.
    * For each transport, the remote message with the highest version is stored.
* In the unshared group, the local message with the highest version is stored.

### Sharing policy

* In the groups shared with contacts, all local messages are shared.
* In the unshared group, no messages are shared.

### Security properties

Confidentiality, integrity and authenticity are provided by the transport security layer ([BTP](/protocols/BTP.md)). Messages are repudiable, but this is not a security goal and may change in future versions of the client.
