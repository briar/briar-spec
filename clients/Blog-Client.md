# Blog Client

The blog client is a [BSP client](/protocols/BSP.md) that synchronises blog posts among groups of devices. It is used in conjunction with the [blog sharing client](blog sharing client).

The creator of a blog is the only user who can post messages. Posts and comments from other blogs can be reblogged with optional comments.

A blog may consist of posts imported from an RSS feed.

### Identifier

The client's identifier is `org.briarproject.briar.blog`. The major version is 0.

### Groups

Each blog is represented by a separate BSP group. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) with two elements: `author` (list) and `rss` (boolean).

`author` is a list with three elements: `formatVersion` (int), `nickname` (string) and `publicKey` (raw). This identifies the user who publishes the blog.

`rss` indicates whether the blog contains an imported RSS feed or a user's personal blog. `nickname` is used as the blog's title. Posts are signed with the private key corresponding to `publicKey`.

### Message types

**0: POST** - A blog post. The message body is a BDF list with three elements: `messageType` (int), `text` (string), and `signature` (raw).

The signature covers a BDF list with three elements: `groupId` (unique ID), `timestamp` (int), and `text` (string). The group ID and timestamp are taken from the message header. The public key from the group descriptor is used for verifying the signature. The signature label is `org.briarproject.briar.blog/POST`.

**1: COMMENT** - A pointer to a reblogged post or comment, with an optional comment. The message body is a BDF list with five elements: `messageType` (int), `comment` (string or null), `parentOriginalId` (unique ID), `parentId` (unique ID), and `signature` (raw).

`parentOriginalId` is the ID of a post or comment in this blog or another blog. `parentId` is the ID of this comment's parent, which is a post, comment, wrapped post or wrapped comment in this blog, which had the ID `parentOriginalId` in the blog where the parent was originally posted.

The signature covers a BDF list with five elements: `groupId` (unique ID), `timestamp` (int), `comment` (string or null), `parentOriginalId` (unique ID), and `parentId` (unique ID). The group ID and timestamp are taken from the message header. The public key from the group descriptor is used for verifying the signature. The signature label is `org.briarproject.briar.blog/COMMENT`.

**2: WRAPPED_POST** - A reblogged post from another blog. The message body is a BDF list with five elements: `messageType` (int), `copiedGroupDescriptor` (list), `copiedTimestamp` (int), `copiedText` (string), and `copiedSignature` (raw).

`copiedGroupDescriptor` is the descriptor of the blog where this post was originally posted. `copiedTimestamp`, `copiedText` and `copiedSignature` are copied from the original post. The public key from the copied group descriptor is used for verifying the signature. The signature label is `org.briarproject.briar.blog/POST`.

The original group ID must be calculated, as it is covered by the signature. The original message ID must also be calculated, as it is referenced by comments.

**3: WRAPPED_COMMENT** - A reblogged comment from another blog. The message body is a BDF list with eight elements: `messageType` (int), `copiedGroupDescriptor` (list), `copiedTimestamp` (int), `copiedComment` (string or null), `copiedParentOriginalId` (unique ID), `copiedParentId` (unique ID), `copiedSignature` (raw), and `parentId` (unique ID).

`copiedGroupDescriptor` is the descriptor of the blog where this comment was originally posted. `copiedTimestamp`, `copiedComment`, `copiedParentOriginalId`, `copiedParentId` and `copiedSignature` are copied from the original comment. The public key from the copied group descriptor is used for verifying the signature. The signature label is `org.briarproject.briar.blog/COMMENT`.

The original group ID must be calculated, as it is covered by the signature. The original message ID must also be calculated, as it is referenced by comments.

`parentId` is the ID of this comment's parent, which is a post, comment, wrapped post or wrapped comment in this blog, which had the ID `copiedParentOriginalId` in the blog where the parent was originally posted, and the ID `copiedParentId` in the blog where this comment was originally posted.

### Validity policy

* A post is valid if it is well-formed and has a valid signature.
* A comment is valid if it is well-formed, has a valid signature, and references a valid message of any type in the same blog, with a matching original ID.
* A wrapped post is valid if a valid post can be reconstructed from it.
* A wrapped comment is valid if a valid comment can be reconstructed from it, and it references a valid message of any type in the same blog, with a matching original ID.

### Storage policy

* All messages are stored.

### Sharing policy

* All posts and comments are shared.
* Wrapped posts and wrapped comments are not shared unless they are referenced by a post or comment.

### Security properties

Confidentiality, integrity and authenticity are provided by the transport security layer ([BTP](/protocols/BTP.md)). Posts and comments are signed with the identity key pairs of their authors and are not repudiable. Wrapped posts and comments are not repudiable by their original authors.
