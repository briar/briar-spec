# Private Group Client

The private group client is a [BSP client](/protocols/BSP.md) that synchronises messages among groups of devices. It is used in conjunction with the [private group sharing client](private group sharing client).

The creator of each private group is the only user who can invite other members. Any member can post messages to a group. Messages are signed by their authors.

### Identifier

The client's identifier is `org.briarproject.briar.privategroup`. The major version is 0.

### Groups

Each private group is represented by a separate BSP group. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) with three elements: `creator` (list), `name` (string), and `salt` (raw).

`creator` is a list with three elements: `formatVersion` (int), `nickname` (string), and `publicKey` (raw). This identifies the user who created the private group. The salt is 32 random bytes, to prevent collisions between private groups with the same creator and name.

### Message types

**0: JOIN** - Sent by a new member to announce that she has joined the private group. The message body is a BDF list with four elements: `messageType` (int), `member` (list), `invite` (list or null), and `memberSignature` (raw).

`member` is a list with three elements: `formatVersion` (int), `nickname` (string), and `publicKey` (raw). This identifies the user who has joined the private group.

`invite` is null if the member created the private group. Otherwise it is a list with two elements: `inviteTimestamp` (int) and `inviteSignature` (raw). These are copied from the [invitation](Private-Group-Sharing-Client#message-types) sent by the creator.

`inviteSignature` covers a BDF list with three elements: `inviteTimestamp` (int), `contactGroupId` (unique ID), and `privateGroupId` (unique ID). `contactGroupId` is the ID of the group used by the creator and the member for [private group invitations](Private-Group-Sharing-Client#group-identifiers) - it can be calculated from the identities of the creator and the member. The public key from the private group descriptor is used to verify `inviteSignature`. The signature label is `org.briarproject.briar.privategroup.invitation/INVITE`.

`memberSignature` covers a BDF list with four elements: `privateGroupId` (unique ID), `timestamp` (int), `member` (list), and `invite` (list or null). The group ID and timestamp are taken from the message header. The public key from `member` is used to verify `memberSignature`. The signature label is `org.briarproject.briar.privategroup/JOIN`.

**1: POST** - The message body is a BDF list with six elements: `messageType` (int), `member` (list), `parentId` (unique ID or null), `previousMessageId` (unique ID), `text` (string), and `signature` (raw).

`member` is a list with three elements: `formatVersion` (int), `nickname` (string), and `publicKey` (raw). This identifies the author of the message.

`parentId` is the optional ID of a message in the same private group to which this message replies. `previousMessageId` is the ID of the previous join or post message sent by this member to this private group.

The signature covers a BDF list with six elements: `groupId` (unique ID), `timestamp` (int), `member` (list), `parentId` (unique ID or null), `previousMessageId` (unique ID), and `text` (string). The group ID and timestamp are taken from the message header. The public key from `member` is used to verify the signature. The signature label is `org.briarproject.briar.privategroup/POST`.

### Validity policy

* A join is valid if it is well-formed, has a valid signature from the new member, and has a valid signature from the creator unless the new member is the creator.
* A post is valid if it is well-formed, has a valid signature, its parent (if any) is a valid post, and its previous message is a valid join or post by the same member.

### Storage policy

* All messages are stored.

### Sharing policy

* All messages are shared.

### Security properties

Confidentiality, integrity and authenticity are provided by the transport security layer ([BTP](/protocols/BTP.md)). A join message is signed with the identity key pair of the member and is not repudiable. The `invite` element of a join message is signed with the identity key pair of the group creator and is not repudiable. Posts are signed with the identity key pair of their authors and are not repudiable.
