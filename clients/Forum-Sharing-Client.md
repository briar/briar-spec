# Forum Sharing Client

The forum sharing client is a [BSP client](/protocols/BSP.md) that allows users to share forums with their contacts, who may accept or decline the invitations. It is used in conjunction with the [forum client](forum client).

### Identifier

The client's identifier is `org.briarproject.briar.forum.sharing`. The major version is 0.

### Groups

The client uses a separate BSP group for communicating with each contact. The [group descriptor](/protocols/BSP.md#23-groups) is a [BDF list](/BDF.md) containing the unique IDs of the contacts' identities, sorted in ascending order as byte strings.

### Roles

All communication happens between two contacts, who have symmetrical roles.

### Sessions

The messages exchanged between two contacts referring to a given forum constitute a session. The forum's unique ID is used as the session ID.

### Message types

The protocol uses five message types.

**0: INVITE** - Sent by either party to start or restart a session. The message body is a BDF list with four elements: `messageType` (int), `previousMessageId` (unique ID or null), `descriptor` (list), and `text` (string or null).

`previousMessageId` is the ID of the previous message in this session, if any. `descriptor` is the [descriptor of the forum](Forum-Client#group-identifiers) being shared. The forum ID must be calculated from the descriptor, as it is used by subsequent messages in the session. `text` is an optional message from the inviter to the invitee explaining the invitation.

The sender sets the forum's visibility to VISIBLE when sending an invite message.

**1: ACCEPT** - Sent in response to an invite. The message body is a BDF list with three elements: `messageType` (int), `forumId` (unique ID), and `previousMessageId` (unique ID).

`previousMessageId` is the ID of the previous message in this session.

The sender sets the forum's visibility to SHARED when sending an accept message. The recipient sets the forum's visibility to SHARED when receiving an accept message.

**2: DECLINE** - Sent in response to an invite. The message body is a BDF list with three elements: `messageType` (int), `forumId` (unique ID), and `previousMessageId` (unique ID).

`previousMessageId` is the ID of the previous message in this session.

The recipient sets the forum's visibility to INVISIBLE when receiving a decline message.

**3: LEAVE** - Sent by either party when unsubscribing from the forum, if the forum's visibility is VISIBLE or SHARED. The message body is a BDF list with three elements: `messageType` (int), `forumId` (unique ID), and `previousMessageId` (unique ID).

`previousMessageId` is the ID of the previous message in this session.

The sender sets the forum's visibility to INVISIBLE when sending a leave message. The recipient sets the forum's visibility to INVISIBLE when receiving a leave message.

**4: ABORT** - Sent by either party when recieving a message that is valid but unexpected in the current state. The message body is a BDF list with three elements: `messageType` (int), `forumId` (unique ID), and `previousMessageId` (unique ID).

`previousMessageId` is the ID of the previous message in this session.

The sender sets the forum's visibility to INVISIBLE when sending an abort message. The recipient sets the forum's visibility to INVISIBLE when receiving an abort message.

### State machine

![state-machine-4](/assets/clients/Forum-Sharing-Client/state-machine-4.png)

[state-machine-4.odg](/assets/clients/Forum-Sharing-Client/state-machine-4.odg)

Aborting from any state returns the session to the START state.

### Validity policy

* A message is valid if it is well-formed and its previous message (if any) is a valid message in the same session.

### Storage policy

* All messages are stored.

### Sharing policy

* All local messages are shared.

### Security properties

Confidentiality, integrity and authenticity are provided by the transport security layer ([BTP](/protocols/BTP.md)). Messages are repudiable, but this is not a security goal and may change in future versions of the client.
